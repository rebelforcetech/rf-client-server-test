var express = require('express');
var app = express();
var handlebars = require('handlebars');

app.use(express.static('client'));

var server = app.listen(5000, function () {
    console.log('Your server is running at https://localhost:5000');
});
